provision_script = <<-SCRIPT
yum install --assumeyes emacs-nox  
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provision 'shell', inline: provision_script
end
