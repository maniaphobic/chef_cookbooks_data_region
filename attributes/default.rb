# Cookbook:: data_region
# Attribute:: File:: default

# The default 
node.default['data_region'] = {
  'bags' => {},
  'config_path' => File.join(
    Chef::Config['client_d_dir'], 'data_region.rb'
  )
}
