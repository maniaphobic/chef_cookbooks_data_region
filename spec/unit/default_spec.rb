require 'spec_helper'

describe 'data_region::default' do
  let(:chef_run) do
    ChefSpec::SoloRunner.new(platform: 'centos', version: '7.2.1511') do
    end.converge(described_recipe)
  end

  it 'converges' do
    expect { chef_run }.not_to raise_error
  end
end
