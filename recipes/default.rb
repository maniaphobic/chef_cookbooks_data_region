# Install the data region gem
chef_gem 'chef_data_region' do
  compile_time true
end

# Manage Chef Client's client.d directory
directory Chef::Config['client_d_dir'] do
  action    :create
  recursive true
end

# Manage the data region config file via its template
template node['data_region']['config_path'] do
  action :create
  source node['data_region']['config_path'].gsub(%r{^/}, '') + '.erb'
  variables(bags: node['data_region']['bags'])
end
