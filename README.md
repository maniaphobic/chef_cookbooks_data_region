# Data Region

## Description

This cookbook configures the [Chef data region
gem](https://bitbucket.org/maniaphobic/ruby_gems_chef_data_region). The
gem replaces Chef Client DSL's `data_bag_item` method with a method
that dynamically expands generic data bag names into region-specific
names. This is useful when one manages a set of data bags whose names
begin with the same prefix and end in region-specific suffixes.

## Requirements

Add `recipe[data_region::default]` to your nodes' run lists,
preferably early. If you use the `chef-client::config` recipe to
manage your Chef Client configuration files, you may wish to place
`data_region::default` immediately after it.

This cookbook depends on no others.

## Attributes

The cookbook's attributes are rooted at `node['data_region']`.

For brevity, this document refers to attributes using a
slash-separated notation that mimics Unix directory paths. Examples
  
### Data bag definitions

`data_region/bags` is a hash defining the bag names to expand. The
hash is empty by default.

The data region gem expects each entry to be a hash in this format:

    {
	  <generic bag name> => {
        'attribute' => <attribute path array>,
		'pattern'   => <bag name expansion pattern>
	  }
	}

The components are as follows:

  * `<generic bag name>`: a string representing the bag's generic
    name. The gem matches data bag names against this string.

  * `<attribute path array>`: an array of strings representing a path
    in node attribute space. When `data_bag_item` matches a generic
    bag name, it substitutes the value at this path into the pattern,
    then attempts to access a data bag with this name.

  * `<bag name expansion pattern>`: a string containing an
    interpolation pattern as understood by Ruby's `Kernel#format`
    method. `data_bag_item` will replace occurrences of
    `%<attribute>s` with the value of `<attribute path array>`.

An example:

    node['data_region']['bags']['secure'] = {
      'attribute' => %w(local region),
	  'pattern' => 'secure_%<attribute>s'
	}

Assume the value of `node['local']['region']` is `staging` and some
recipe contains this `data_bag_item` invocation:

    credentials = data_bag_item('secure', 'service')

`data_bag_item` would expand the pattern with the value of
`local/regiion`, resulting in this effective invocation:

    credentials = data_bag_item('secure_staging', 'service')

### Configuration path definition

The `data_region/config_path` attribute specifies a path to a Chef
Client configuration file that configures the data region gem. Its
default value is `/etc/chef/client.d/data_region.rb`.
