chef_version '> 11'
description File.read('README.md')
issues_url 'https://bitbucket.org/maniaphobic/chef_cookbooks_data_region'
license '0BSD'
maintainer 'Someone'
maintainer_email 'so@me.one'
name 'data_region'
recipe 'data_region::default', 'Configure the data region gem'
source_url 'https://bitbucket.org/maniaphobic/chef_cookbooks_data_region'
version '1.0.0'

{
  'centos' => '> 4',
  'ubuntu' => '> 9'
}.each do |platform, version|
  supports platform, version
end
